# Desafio Análise de Crédito

## Configurações
* Criar um banco de dados utilizando MySQL
* Realizar configurações do banco no arquivo "application.properties"
```
Ex:
spring.datasource.url=jdbc:mysql://127.0.0.1:3306/sistema_analise?serverTimezone=UTC&useLegacyDatetimeCode=false
spring.datasource.username=username
spring.datasource.password=password
```
* Ao executar a aplicação, registros serão inseridos no banco
* Acessar o Swagger
```
Ex: http://localhost:8080/swagger-ui.html
```

## Swagger - Rotas
### Portador
##### POST /api/portador
Cadastro de portador e sua proposta (permitido apenas para usuário captador)

##### Parâmetro
* Id do Usuário: 1 (Analista) ou 2 (Captador)

##### Body
```
{
  "cpf": "83474921827",
  "nome": "Nina Alessandra",
  "proposta": {
    "credito": 890
  }
}
```
##### Response
```
{
  "id": 8,
  "nome": "Nina Alessandra",
  "cpf": "83474921827",
  "proposta": {
    "id": 8,
    "credito": 890,
    "status": "pendente"
  }
}
```

### Proposta
##### GET /api/proposta
Busca por propostas de acordo com a permissão do usuário

Analista: retorna apenas propostas pendentes
Captador: retorna apenas propostas analisadas

##### Parâmetros
* Id do Usuário: 1 (Analista) ou 2 (Captador)
* Crédito Mínimo: filtra por propostas com crédito maior igual
* Crédito Máximo: filtra por propostas com crédito menor igual

##### Response

```
{
  "id": 8,
  "nome": "Nina Alessandra",
  "cpf": "83474921827",
  "proposta": {
    "id": 8,
    "credito": 890,
    "status": "pendente"
  }
}
...
```

#### PUT /api/proposta/{id}
Atualiza status da proposta (permitido apenas para usuário analista)

##### Parâmetros
* proposta_id: ID da proposta
* Id do Usuário: 1 (Analista) ou 2 (Captador)

##### Body
```
"APROVADO"
```

##### Response
```
{
  "id": 8,
  "credito": 890,
  "status": "aprovado"
}
```