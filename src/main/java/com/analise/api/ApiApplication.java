package com.analise.api;

import java.math.BigDecimal;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.analise.api.model.Portador;
import com.analise.api.model.Proposta;
import com.analise.api.model.Usuario;
import com.analise.api.model.enums.Permissao;
import com.analise.api.model.enums.Status;
import com.analise.api.repository.PortadorRepository;
import com.analise.api.repository.UsuarioRepository;

@SpringBootApplication
public class ApiApplication implements CommandLineRunner {
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	private PortadorRepository portadorRepository;

	public static void main(String[] args) {
		SpringApplication.run(ApiApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		List<Usuario> usuarios = usuarioRepository.findAll();
		if (usuarios.isEmpty()) {
			MessageDigest md = MessageDigest.getInstance("MD5");		
			Usuario u1 = new Usuario(1, "Jorge Noah", "jorge.noah", "drnhsdf", Permissao.ANALISTA);
			md.update(u1.getSenha().getBytes());
			u1.setSenha(md.digest().toString());

			Usuario u2 = new Usuario(2, "Martin Heitor", "martin.heitor", "sdffghd", Permissao.CAPTADOR);
			md.update(u2.getSenha().getBytes());
			u2.setSenha(md.digest().toString());

			usuarioRepository.saveAll(Arrays.asList(u1, u2));
		}

		List<Portador> portadores = portadorRepository.findAll();
		if (portadores.isEmpty()) {
			Proposta pr1 = new Proposta(1, new BigDecimal(850), Status.PENDENTE);
			Proposta pr2 = new Proposta(2, new BigDecimal(1230), Status.PENDENTE);
			Proposta pr3 = new Proposta(3, new BigDecimal(960), Status.PENDENTE);
			Proposta pr4 = new Proposta(4, new BigDecimal(1500), Status.APROVADO);
			Proposta pr5 = new Proposta(5, new BigDecimal(700), Status.APROVADO);
			Proposta pr6 = new Proposta(6, new BigDecimal(630), Status.NEGADO);
			Proposta pr7 = new Proposta(7, new BigDecimal(1350), Status.NEGADO);

			Portador p1 = new Portador(1, "Giovanna Rosângela", "75966993831", pr1);
			Portador p2 = new Portador(2, "Giovanni Enzo", "94269045493", pr2);
			Portador p3 = new Portador(3, "Emilly Sandra", "83672090794", pr3);
			Portador p4 = new Portador(4, "Manuel Davi", "35870264758", pr4);
			Portador p5 = new Portador(5, "Davi Sebastião", "90184347106", pr5);
			Portador p6 = new Portador(6, "Isabella Laís", "35117831519", pr6);
			Portador p7 = new Portador(7, "Miguel Bernardo", "97699929845", pr7);
			
			portadorRepository.saveAll(Arrays.asList(p1, p2, p3, p4, p5, p6, p7));
		}
	}
}
