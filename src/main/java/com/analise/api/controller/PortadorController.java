package com.analise.api.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.analise.api.model.Portador;
import com.analise.api.service.PortadorService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

//import io.swagger.annotations.Api;

@RestController
@RequestMapping(value = "/api/portador")
@Api(value = "API")
@CrossOrigin(origins = "*")
public class PortadorController {

	@Autowired
	PortadorService portadorService;

	@ApiOperation(value = "Cadastra Portador")
	@RequestMapping(method = RequestMethod.POST, produces="application/json")
	@ResponseStatus(HttpStatus.CREATED)
	public Portador cadastrarPortador (
			@Valid
			@ApiParam(value = "ID 1 para Analista e 2 para Captador", required = true)
			@RequestParam(value = "Id do Usuário") long usuario_id,

			@ApiParam(value = "Dados do Portador", required = true)
			@RequestBody Portador portador) throws Exception {

		return portadorService.cadastrarPortador(portador, usuario_id);
	}
}
