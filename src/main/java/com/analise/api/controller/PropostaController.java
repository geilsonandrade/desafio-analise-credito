package com.analise.api.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.analise.api.model.Portador;
import com.analise.api.model.Proposta;
import com.analise.api.model.enums.Status;
import com.analise.api.service.PropostaService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value = "/api/proposta")
public class PropostaController {

	@Autowired
	PropostaService propostaService;

	@ApiOperation(value = "Lista Propostas")
	@RequestMapping(method = RequestMethod.GET, produces="application/json")
	@ResponseStatus(HttpStatus.OK)
	public List<Portador> listarPropostas(
			@ApiParam(value = "ID 1 para Analista e 2 para Captador", required = true)
			@RequestParam(value = "Id do Usuário") long usuario_id,
				
			@ApiParam(value = "Filtrar por crédito mínimo")
			@RequestParam(required = false, value = "Crédito Mínimo") Float minCredito, 
			
			@ApiParam(value = "Filtrar por crédito máximo")	
			@RequestParam(required = false, value = "Crédito Máximo") Float maxCredito) throws Exception {
		
		return propostaService.listarPortadores(minCredito, maxCredito, usuario_id);
	}

	@ApiOperation(value = "Atualiza Status da Proposta")
	@RequestMapping(method = RequestMethod.PUT, value = "/{proposta_id}", produces="application/json")
	@ResponseStatus(HttpStatus.OK)
	public Proposta atualizarProposta(
			@Valid
			@PathVariable(value = "proposta_id") long proposta_id,

			@ApiParam(value = "ID 1 para Analista e 2 para Captador", required = true)
			@RequestParam(value = "Id do Usuário") long usuario_id,

			@ApiParam(value = "Status da Proposta (PENDENTE, APROVADO, NEGADO)", required = true)
			@RequestBody Status status) throws Exception {

		return propostaService.atualizarStatusProposta(proposta_id, status, usuario_id);
	}
}
