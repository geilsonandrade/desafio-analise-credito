package com.analise.api.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Min;

import com.analise.api.model.enums.Status;
import com.sun.istack.NotNull;

@Entity
public class Proposta implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@NotNull
	@Min(0)
	@Column(nullable=false)
	private BigDecimal credito;

	@NotNull
	@Column(nullable=false)
	@Enumerated(EnumType.STRING)
	private Status status;

	public Proposta() {
		super();
	}
	
	public Proposta(long id, Status status) {
		super();
		this.id = id;
		this.status = status;
	}

	public Proposta(long id, BigDecimal credito, Status status) {
		//super();
		this.id = id;
		this.credito = credito;
		this.status = status;
	}

	public BigDecimal getCredito() {
		return credito;
	}

	public void setCredito(BigDecimal credito) {
		this.credito = credito;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
}
