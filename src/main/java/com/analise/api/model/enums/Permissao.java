package com.analise.api.model.enums;


public enum Permissao {
	ANALISTA("analista"),
	CAPTADOR("captador");

	private String permissao_usuario;

	Permissao(String permissao_usuario) {
		this.permissao_usuario = permissao_usuario;
	}
	
	public String getPermissaoUsuario() {
		return permissao_usuario;
	}
}
