package com.analise.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.analise.api.model.Portador;

@Repository
public interface PortadorRepository extends JpaRepository<Portador, Long> {

	@Query(value = "SELECT * FROM portador p "
			+ "INNER JOIN proposta p2 ON p.proposta_id = p2.id "
			+ "WHERE p2.status != 'PENDENTE' "
			+ "AND p2.credito BETWEEN ?1 AND ?2", nativeQuery = true)
	List<Portador> findByAnalysisResult(float minCred, float maxCred);

	@Query(value = "SELECT * FROM portador p "
			+ "INNER JOIN proposta p2 ON p.proposta_id = p2.id "
			+ "WHERE p2.status = 'PENDENTE' "
			+ "AND p2.credito BETWEEN ?1 AND ?2", nativeQuery = true)
	List<Portador> findByStatusPending(float minCred, float maxCred);
	
	Portador findByCpf(String cpf);
}
