package com.analise.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.analise.api.model.Portador;
import com.analise.api.model.enums.Permissao;
import com.analise.api.model.enums.Status;
import com.analise.api.repository.PortadorRepository;

@Service
public class PortadorService {

	@Autowired
	PortadorRepository portadorRepository;

	@Autowired
	UsuarioService usuarioService;

	public List<Portador> resultadoAnalise (float minCred, float maxCred) {
		return portadorRepository.findByAnalysisResult(minCred, maxCred);
	}
	
	public List<Portador> statusPendente (float minCred, float maxCred) {
		return portadorRepository.findByStatusPending(minCred, maxCred);
	}
	
	public Portador cadastrarPortador (Portador portador, long usuario_id) {
		if (usuarioService.tipoPermissao(usuario_id, Permissao.CAPTADOR)) {
			if (portadorRepository.findByCpf(portador.getCpf()) != null) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
						"Não foi possível realizar o cadastro. Portador com esse CPF já existe");
			} else {
				portador.getProposta().setStatus(Status.PENDENTE);
				return portadorRepository.save(portador);
			}
		} else {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,
					"Usuario não autorizado"); 
		}
	}
}