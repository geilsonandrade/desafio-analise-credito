package com.analise.api.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.analise.api.model.Portador;
import com.analise.api.model.Proposta;
import com.analise.api.model.enums.Permissao;
import com.analise.api.model.enums.Status;
import com.analise.api.repository.PropostaRepository;

@Service
public class PropostaService {

	@Autowired
	PropostaRepository propostaRepository;

	@Autowired
	UsuarioService usuarioService;
	
	@Autowired
	PortadorService portadorService;

	public List<Portador> listarPortadores (Float minCredito, Float maxCredito, long usuario_id) throws Exception {
		float minCred = 0;
		float maxCred = 99999;

		if (minCredito != null) {
			minCred = minCredito;
		}

		if (maxCredito != null) {
			maxCred = maxCredito;
		}

		List<Portador> portadores = null;

		if (usuarioService.tipoPermissao(usuario_id, Permissao.CAPTADOR)) {
			portadores = portadorService.resultadoAnalise(minCred, maxCred);
		} else if (usuarioService.tipoPermissao(usuario_id, Permissao.ANALISTA)) {
			portadores = portadorService.statusPendente(minCred, maxCred);
		} else {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,
					"Usuario não autorizado"); 
		}
		
		if (portadores.isEmpty()) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND,
					"Nenhuma proposta encontrada");
		} else {
			return portadores;
		}
	}
	
	public Proposta atualizarStatusProposta (long proposta_id, Status status, long usuario_id) {
		Optional<Proposta> ant_proposta = propostaRepository.findById(proposta_id);
		
		if (usuarioService.tipoPermissao(usuario_id, Permissao.ANALISTA)) {
			ant_proposta.get().setStatus(status);
			return propostaRepository.save(ant_proposta.get());
		} else {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,
					"Usuario não autorizado");
		}
	}
}
