package com.analise.api.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.analise.api.model.Usuario;
import com.analise.api.model.enums.Permissao;
import com.analise.api.repository.UsuarioRepository;

@Service
public class UsuarioService {

	@Autowired
	UsuarioRepository usuarioRepository;
	
	public boolean tipoPermissao (long usuario_id, Permissao permissao) {
		Optional<Usuario> usuario = usuarioRepository.findById(usuario_id);

		if (usuario.isPresent() && usuario.get().getPermissao() == permissao) {
			return true;
		} else {
			return false;
		}
	}
}
